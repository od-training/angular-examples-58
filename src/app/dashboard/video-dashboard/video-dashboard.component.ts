import { Component } from '@angular/core';
import { Video } from '../types';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;

  constructor(svc: VideoDataService) {
    this.videoList = svc.loadVideos();
  }
}
