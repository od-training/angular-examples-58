import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../types';
import { Observable } from 'rxjs';
import { VideoDataService } from 'src/app/video-data.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  selectedVideoId: Observable<string>;
  @Input() videos: Video[];
  @Output() pickVideo = new EventEmitter<Video>();

  constructor(private vds: VideoDataService, route: ActivatedRoute) {
    this.selectedVideoId = route.paramMap.pipe(
      map(paramMap => paramMap.get('employeeId') as string)
    );
  }

  selectVideo(video: Video) {
    // this.vds.setCurrentVideo(video);
  }
}
