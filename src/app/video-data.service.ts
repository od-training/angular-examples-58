import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from './dashboard/types';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  loadVideos() {
    return this.http
      .get<Video[]>(apiUrl + '/videos')
      .pipe(map(convertTitleToUppercase));
  }

  getDetails(id: string) {
    return this.http.get<Video>(`${apiUrl}/videos/${id}`);
  }
}

function convertTitleToUppercase(videos: Video[]) {
  return videos.map(v => ({ ...v, title: v.title.toUpperCase() }));
}
