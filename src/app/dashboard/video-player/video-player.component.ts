import { Component } from '@angular/core';
import { Video } from '../types';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap, filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent {
  video: Observable<Video>;

  constructor(route: ActivatedRoute, vds: VideoDataService) {
    this.video = route.paramMap.pipe(
      map(paramMap => paramMap.get('videoId')),
      filter((id: string | null): id is string => !!id),
      switchMap(id => vds.getDetails(id))
    );
  }
}
